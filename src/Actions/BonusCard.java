package Actions;

import mainP.*;

/**
 * Created by artmmslv on 31.12.2016.
 */
public class BonusCard extends Action{
    //todo: вопрос в том, что будет если есть право выкладывать карты, но колода пуста и появляется новая карта
    public BonusCard(Game game){super(game);}

    @Override
    public Action same() {
        return new BonusCard(game);
    }

    @Override
    public void realize() {
            game.current.getPlayer().addCardToHand();
    }
}

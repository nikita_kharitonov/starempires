package Actions;

import mainP.Action;
import mainP.Game;

/**
 * Created by artmmslv on 31.12.2016.
 */
public class CollectDamage extends Action {
    int amount;
    public CollectDamage(int amount, Game game){
        super(game);
        this.amount = amount;

    }
    @Override
    public void realize() {
        {
            game.getDamage().add(amount);
        }
    }

    @Override
    public Action same() {
        return new CollectDamage(amount, game);
    }
}

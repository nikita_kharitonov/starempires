package Actions;

import mainP.Action;
import mainP.Game;

/**
 * Created by artmmslv on 31.12.2016.
 */
public class CollectTrade extends Action{
    public int amount;
    public CollectTrade(int amount,Game game){
        super(game);
        this.amount = amount;
    }

    public void realize(){
            game.getGold().add(amount);

    }

    @Override
    public Action same() {
        return new CollectTrade(amount, game);
    }
}

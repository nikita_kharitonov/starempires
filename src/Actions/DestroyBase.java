package Actions;

import mainP.*;

/**
 * Created by Никиа on 06.01.2017.
 */
public class DestroyBase extends TwoStepAction {
    public DestroyBase(Game g){
        super(g);
    }

    @Override
    public Action same() {
        return new DestroyBase(game);
    }

    @Override
    public void realize(Card card) {
        game.enemyBaseToFold(card);
    }

    @Override
    protected boolean fits(Card card) {
        if(game.whoseEnemyBase(card)>=0)return true;
        return false;
    }
}

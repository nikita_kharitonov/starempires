package Actions;

import mainP.*;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class Discard1 extends TwoStepAction {
    public Discard1(Game g){
        super(g);
    }

    @Override
    public Action same() {
        return new Discard1(game);
    }

    protected void realize(Player player){
        player.discard2.addCard();
    }
    protected boolean fits(Player player){
        return !player.isCurrent();
    }
    public void finish(Player player){
        if(fits(player)){
            realize(player);
            setActive(false);
            setUsed(true);
            game.setActive(null);
            game.cardUtilisation(cardToDestroy);
        }else{
            decline();
        }
    }
    @Override
    protected void realize(Card card) {

    }

    @Override
    protected boolean fits(Card card) {
        return false;
    }
}

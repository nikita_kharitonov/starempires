package Actions;

import mainP.*;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class Discard2 extends TwoStepAction{//Спецдействие в плеере, заставляет бросать карты. Не действие карты
                                //Не вполне Action, но похож на него
    private int cardAmount = 0;
    private Player player;
    public Discard2(Player p, Game game){
        super(game);
        player = p;
        this.game = game;
    }
    public boolean launch(){
        if(!isActive()){
            disableOrEnableTable(false);
            game.setActive(null);
        }else{
            game.setActive(this);
            disableOrEnableTable(true);
        }
        return true;
    }
    public void finish(Card card){
        if(fits(card)){
            realize(card);
            launch();
        }
        else game.log("Карта не подходит");
    }
    public boolean fits(Card card){
        if(player.getBases().contains(card)||player.getHand().contains(card))return true;
        return false;
    }
    public void realize(Card card){
        cardAmount--;
        game.tableToFold(card);
    }
    public void disableOrEnableTable(boolean newState){
        for (int i = 0; i < player.getHand().size(); i++) {
            Card card = player.getHand().get(i);
            card.setDisabled(newState);
        }
        for (int i = 0; i < player.getBases().size(); i++) {
            Card card = player.getBases().get(i);
            card.setDisabled(newState);
        }
    }

    @Override
    public Action same() {
        return null;
    }

    public boolean isActive(){
        return !(cardAmount == 0 || player.getHand().size()+player.getBases().size()==0);
    }
    public void addCard(){
        cardAmount++;
    }
}

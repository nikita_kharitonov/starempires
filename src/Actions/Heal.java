package Actions;

import mainP.*;

/**
 * Created by artmmslv on 31.12.2016.
 */
public class Heal extends Action{
    int amount;
    public Heal(int amount,Game game){
        super(game);
        this.amount = amount;
    }
    @Override
    public void realize() {
            game.current.getPlayer().hp.add(amount);
    }

    @Override
    public Action same() {
        return new Heal(amount, game);
    }
    /*Добавляем хил. За 50 залезать можно.*/
}

package Actions;

import Ships.StealthNeedle;
import mainP.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class NeedleAction extends TwoStepAction {

    protected Card origin;
    protected Card needleSelf;
    protected ArrayList<ActionArea> stolenAreas;
    public NeedleAction(Game g, Card stealthNeedle){
        super(g);
        needleSelf = stealthNeedle;
    }
    public void coolDown(){
        setUsed(false);
        stolenAreas = null;
        origin = null;
    }
    @Override
    protected void realize(Card card) {
        try{
            this.origin = card;
            stolenAreas = stealAreas(origin);
            //У действий есть взаимосвязи OR, они скопировались на новые действия//todo проверить!
            //Но действия удаления будут действовать на origin
            //Сейчас исправим
            Card.setUtilCard(stolenAreas, needleSelf);
            this.setDisabled(true);
            game.combos.refresh();
            inform();
        } catch (Exception e){
            System.err.println("NeedleAction failed");
            e.printStackTrace();
            decline();
        }
    }

    protected ArrayList<ActionArea> stealAreas(Card origin){
        ArrayList<ActionArea> stolenList = new ArrayList<>();
        ArrayList<ActionArea> originalAreas = origin.getAreas();
        //Поступаем как кукушка - заставляем карту origin заполнять нам массив
        origin.setAreas(stolenList);
        origin.addActions();//addActions добавляет действия в поле areas,которое мы подменили
        origin.setAreas(originalAreas);//Так и было, честное слово!
        return stolenList;
    }
    public boolean hasStolen(){
        return origin != null;
    }

    @Override
    protected boolean fits(Card card) {
        return game.current.getHand().contains(card) && !(card instanceof StealthNeedle);
    }
    @Override
    public NeedleAction same(){
        return new NeedleAction(game, needleSelf);
    }

    public Card getOrigin() {
        return origin;
    }


    public ArrayList<ActionArea> getStolenAreas() {
        return stolenAreas;
    }
}

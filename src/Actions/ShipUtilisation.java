package Actions;

import mainP.*;

/**
 * Created by Никиа on 06.01.2017.
 */
public class ShipUtilisation extends TwoStepAction {//С руки или сброса
    public ShipUtilisation(Game game){
        super(game);
    }

    @Override
    public Action same() {
        return new ShipUtilisation(game);
    }

    @Override
    protected void realize(Card card) {
        game.cardUtilisation(card);
    }

    @Override
    protected boolean fits(Card card) {
        if(game.current.getHand().contains(card))return true;
        if(game.current.getFold().contains(card))return true;
        return false;
    }

}

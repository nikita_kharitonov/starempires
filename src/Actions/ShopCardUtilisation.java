package Actions;

import mainP.*;

/**
 * Created by Никиа on 06.01.2017.
 */
public class ShopCardUtilisation extends TwoStepAction {
    public ShopCardUtilisation(Game game){
        super(game);
    }

    @Override
    public Action same() {
        return new ShopCardUtilisation(game);
    }

    @Override
    protected void realize(Card card) {
        game.dropShop(card);
    }

    @Override
    protected boolean fits(Card card) {
        return game.getShop().contains(card);
    }


}

package Actions;

import mainP.Action;
import mainP.Card;
import mainP.Game;
import mainP.TwoStepAction;

/**
 * Created by artmmslv on 17.01.2017.
 */
public class freeShipToDeck extends TwoStepAction {//Слизненосец и таможенный корабль
    int maxCost;
    public freeShipToDeck(int maxCost, Game game) {
        super(game);
        this.maxCost = maxCost;
    }

    @Override
    protected void realize(Card card) {
        game.shopToStock(card);
    }

    @Override
    protected boolean fits(Card card) {
        return game.getShop().contains(card) && card.getCost() <= maxCost;
    }

    @Override
    public Action same() {
        return new freeShipToDeck(maxCost, game);
    }
}

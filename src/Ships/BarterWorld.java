package Ships;

import Actions.CollectDamage;
import Actions.CollectTrade;
import Actions.Heal;
import mainP.*;


/**
 * Created by artmmslv on 18.01.2017.
 */
public class BarterWorld extends Card {
    public BarterWorld(Game g){
        super(g, 4, Race.BLUE);
        thisIsBase(false,4);
    }

    @Override
    public void addActions() {
        Action Heal = new Heal(2,game);
        Action Trade = new CollectTrade(2, game);
        Heal.addOr(Trade);
        addUsualAction(Heal, FloatRec.base2Left);
        addUsualAction(Trade,FloatRec.base2Right);
        addUtilAction(new CollectDamage(5,game),FloatRec.base1);
    }
}

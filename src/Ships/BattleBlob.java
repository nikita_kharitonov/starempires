package Ships;

import Actions.BonusCard;
import Actions.CollectDamage;
import mainP.*;

/**
 * Created by Никиа on 05.01.2017.
 */
public class BattleBlob extends Card {
    public BattleBlob(Game game){
        super(game,6, Race.GREEN);

    }


    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(8,game), FloatRec.ship3);
        addComboAction(new BonusCard(game), FloatRec.ship2, game.combos.getGreen());
        addUtilAction(new CollectDamage(4,game), FloatRec.ship1);
    }
}

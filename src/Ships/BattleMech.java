package Ships;

import Actions.BonusCard;
import Actions.CollectDamage;
import Actions.ShipUtilisation;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 17.01.2017.
 */
public class BattleMech extends Card {
    public BattleMech(Game game){
        super(game,5,Race.RED);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(4,game), FloatRec.ship3);
        addUsualAction(new ShipUtilisation(game),FloatRec.ship2);
        addComboAction(new BonusCard(game),FloatRec.ship1,game.combos.getRed());
    }
}

package Ships;

import Actions.CollectDamage;
import Actions.ShopCardUtilisation;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 17.01.2017.
 */
public class BattlePod extends Card {
    public BattlePod(Game game){
        super(game,2,Race.GREEN);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(4,game), FloatRec.ship3);
        addUsualAction(new ShopCardUtilisation(game),FloatRec.ship2);
        addComboAction(new CollectDamage(2,game),FloatRec.ship1,game.combos.getGreen());
    }
}

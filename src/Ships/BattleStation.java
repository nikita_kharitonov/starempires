package Ships;

import Actions.CollectDamage;
import mainP.*;

/**
 * Created by Никиа on 07.01.2017.
 */
public class BattleStation extends Card {
    public BattleStation(Game game){
        super(game,3,Race.RED);

        thisIsBase(true,5);
    }


    @Override
    public void addActions() {
        addUtilAction(new CollectDamage(5,game), FloatRec.base2);
    }
}

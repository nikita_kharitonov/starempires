package Ships;

import Actions.CollectDamage;
import Actions.freeShipToDeck;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 17.01.2017.
 */
public class BlobCarrier extends Card {
    public BlobCarrier(Game game){
        super(game,6,Race.GREEN);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(7,game), FloatRec.ship3);
        addComboAction(new freeShipToDeck(Integer.MAX_VALUE,game),FloatRec.ship2,game.combos.getGreen());
    }
}

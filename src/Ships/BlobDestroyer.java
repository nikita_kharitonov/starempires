package Ships;

import Actions.CollectDamage;
import Actions.DestroyBase;
import Actions.ShopCardUtilisation;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 17.01.2017.
 */
public class BlobDestroyer extends Card {
    public BlobDestroyer(Game game){
        super(game,4,Race.GREEN);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(6,game), FloatRec.ship3);
        FloatRec base = new FloatRec(FloatRec.linel, FloatRec.line2,FloatRec.width, FloatRec.heigt/3);
        FloatRec shop = new FloatRec(FloatRec.linel,FloatRec.line2+FloatRec.heigt/3,FloatRec.width, 2.0f*FloatRec.heigt/3);
        addComboAction(new DestroyBase(game),base,game.combos.getGreen());
        addComboAction(new ShopCardUtilisation(game),shop,game.combos.getGreen());
    }
}

package Ships;

import Actions.BonusCard;
import Actions.CollectDamage;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by Никиа on 05.01.2017.
 */
public class BlobFighter extends Card {
    public BlobFighter(Game game){
        super(game,1,Race.GREEN);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(3,game), FloatRec.ship3);
        addComboAction(new BonusCard(game),FloatRec.ship2,game.combos.getGreen());
    }
}
package Ships;

import Actions.CollectDamage;
import Actions.CollectTrade;
import mainP.*;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class BlobWheel extends Card{
    public BlobWheel(Game game){
        super(game,3,Race.GREEN);
        thisIsBase(false,5);
    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(1, game), FloatRec.base2);
        addUtilAction(new CollectTrade(3, game),FloatRec.base1);

    }
}

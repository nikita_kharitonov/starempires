package Ships;

import Actions.DestroyBase;
import Actions.BonusCard;
import Actions.CollectDamage;
import Actions.Heal;
import mainP.*;

/**
 * Created by Никиа on 08.01.2017.
 */
public class CommandShip extends Card {
    public CommandShip(Game game) {
        super(game,8,Race.BLUE);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(5, game), FloatRec.ship3Left);
        addUsualAction(new BonusCard(game), FloatRec.ship2Left);
        addUsualAction(new BonusCard(game), FloatRec.ship2Right);
        addUsualAction(new Heal(4,game), FloatRec.ship3Right);
        addComboAction(new DestroyBase(game), FloatRec.ship1, game.combos.getBlue());
    }
}

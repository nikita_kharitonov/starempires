package Ships;

import Actions.BonusCard;
import Actions.CollectDamage;
import mainP.*;

/**
 * Created by Никиа on 05.01.2017.
 */
public class Corvette extends Card {
    public Corvette(Game game){
        super(game,2,Race.YELLOW);

    }


    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(1,game), FloatRec.ship3);
        addUsualAction(new BonusCard(game), FloatRec.ship2);
        Action action = new CollectDamage(2,game);
        addComboAction(action, FloatRec.ship1,game.combos.getYellow());
    }
}

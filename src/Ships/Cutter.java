package Ships;

import Actions.CollectDamage;
import Actions.CollectTrade;
import Actions.Heal;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class Cutter extends Card {
    public Cutter(Game game){
        super(game,2,Race.BLUE);
    }

    @Override
    public void addActions() {
        addUsualAction(new Heal(4,game), FloatRec.ship3Left);
        addUsualAction(new CollectTrade(2,game),FloatRec.ship3Right);
        addComboAction(new CollectDamage(4,game),FloatRec.ship2, game.combos.getBlue());
    }
}

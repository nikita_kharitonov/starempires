package Ships;

import Actions.CollectDamage;
import Actions.Heal;
import mainP.*;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class DefenseCenter extends Card {
    public DefenseCenter(Game game){
        super(game,5, Race.BLUE);
        thisIsBase(true,5);
    }

    @Override
    public void addActions() {
        Action heal = new Heal(3,game);
        Action dmg  = new CollectDamage(3,game);
        heal.addOr(dmg);
        addUsualAction(heal, FloatRec.base2Left);
        addUsualAction(dmg, FloatRec.base2Right);
        addComboAction(new CollectDamage(2,game),FloatRec.base1,game.combos.getBlue());
    }
}

package Ships;

import Actions.BonusCard;
import Actions.CollectDamage;
import mainP.*;

/**
 * Created by artmmslv on 31.12.2016.
 */
public class Dreadnaught extends Card {
    public Dreadnaught(Game g){
        super(g,7,Race.YELLOW);

    }


    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(7,game), FloatRec.ship3);
        addUsualAction(new BonusCard(game), FloatRec.ship2);
        addUtilAction(new CollectDamage(5,game), FloatRec.ship1);
    }
}
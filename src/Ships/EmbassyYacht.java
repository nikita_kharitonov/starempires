package Ships;

import Actions.*;
import mainP.*;
/**
 * Created by artmmslv on 31.12.2016.
 */
public class EmbassyYacht extends Card{
    public EmbassyYacht(Game game){
        super(game,3,Race.BLUE);
    }

    @Override
    public void addActions() {
        addUsualAction(new CollectTrade(2,game), FloatRec.ship3Right);//todo:mark normally
        addUsualAction(new Heal(3,game), FloatRec.ship3Left);
        addComboAction(new BonusCard(game),FloatRec.ship2Left,game.combos.getBases());
        addComboAction(new BonusCard(game),FloatRec.ship2Right,game.combos.getBases());
    }
}

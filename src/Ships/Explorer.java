package Ships;

import Actions.CollectDamage;
import Actions.CollectTrade;
import mainP.*;

/**
 * Created by artmmslv on 31.12.2016.
 */
public class Explorer extends Card {
    public Explorer(Game game){
        super(game,2,Race.NEUTRAL);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectTrade(2,game),FloatRec.Explorer);
        addUtilAction(new CollectDamage(2,game),FloatRec.ship1);
    }
}

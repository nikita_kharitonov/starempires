package Ships;

import Actions.CollectTrade;
import Actions.Heal;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class FederationShuttle extends Card {
    public FederationShuttle(Game game){
        super(game,1,Race.BLUE);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectTrade(2,game), FloatRec.ship3);
        addComboAction(new Heal(4,game),FloatRec.ship2,game.combos.getBlue());
    }
}

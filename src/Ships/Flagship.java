package Ships;

import Actions.BonusCard;
import Actions.CollectDamage;
import Actions.Heal;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class Flagship extends Card {
    public Flagship(Game game){
        super(game,6,Race.BLUE);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(5,game), FloatRec.ship3);
        addUsualAction(new BonusCard(game),FloatRec.ship2);
        addComboAction(new Heal(5,game),FloatRec.ship1,game.combos.getBlue());
    }
}

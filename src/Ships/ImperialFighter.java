package Ships;

import Actions.CollectDamage;
import Actions.Discard1;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class ImperialFighter extends Card {
    public ImperialFighter(Game game){
        super(game, 1, Race.YELLOW);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(2,game), FloatRec.ship3);
        addUsualAction(new Discard1(game),FloatRec.ship2);
        addComboAction(new CollectDamage(2,game),FloatRec.ship1,game.combos.getYellow());
    }
}

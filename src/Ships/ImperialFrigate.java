package Ships;

import Actions.BonusCard;
import Actions.CollectDamage;
import Actions.Discard1;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class ImperialFrigate extends Card {
    public ImperialFrigate(Game game){
        super(game,3, Race.YELLOW);

    }

    @Override
    public void addActions() {
        FloatRec small = new FloatRec(FloatRec.ship3);
        small.setW(0.2f);
        FloatRec big = new FloatRec(FloatRec.ship3);
        big.setX(0.28f);
        big.setW(0.70f);
        addUsualAction(new CollectDamage(4,game),small);
        addUsualAction(new Discard1(game),big);
        addComboAction(new CollectDamage(2,game),FloatRec.ship2,game.combos.getYellow());
        addUtilAction(new BonusCard(game),FloatRec.ship1);
    }
}

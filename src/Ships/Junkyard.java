package Ships;

import Actions.ShipUtilisation;
import mainP.*;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class Junkyard extends Card {
    public Junkyard(Game game){
        super(game,6,Race.RED);

        thisIsBase(true,5);
    }

    @Override
    public void addActions() {
        addUsualAction(new ShipUtilisation(game), FloatRec.base2);
    }
}

package Ships;

import mainP.*;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class MechWorld extends Card implements ComboBonusGranter{
    public MechWorld(Game game){
        super(game,6, Race.RED);
        thisIsBase(true,6);
        //todo Прописать пассивку
    }

    @Override
    public void addCombos(ObservableBoolean yellow, ObservableBoolean blue, ObservableBoolean green, ObservableBoolean red, ObservableBoolean bases) {
        yellow.set(true);
        blue.set(true);
        green.set(true);
        red.set(true);
    }

    @Override
    public void addActions() {

    }
}

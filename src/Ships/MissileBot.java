package Ships;

import Actions.CollectDamage;
import Actions.ShipUtilisation;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class MissileBot extends Card {
    public MissileBot(Game game){
        super(game,2, Race.RED);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(2,game), FloatRec.ship3);
        addUsualAction(new ShipUtilisation(game),FloatRec.ship2);
        addComboAction(new ShipUtilisation(game),FloatRec.ship1,game.combos.getRed());
    }
}

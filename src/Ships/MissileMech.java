package Ships;

import Actions.BonusCard;
import Actions.CollectDamage;
import Actions.DestroyBase;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class MissileMech extends Card {
    public MissileMech(Game game){
        super(game,6, Race.RED);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(6,game), FloatRec.ship3);
        addUsualAction(new DestroyBase(game),FloatRec.ship2);
        addComboAction(new BonusCard(game),FloatRec.ship1,game.combos.getRed());
    }
}

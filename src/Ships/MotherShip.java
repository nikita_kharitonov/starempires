package Ships;

import Actions.BonusCard;
import Actions.CollectDamage;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class MotherShip extends Card {
    public MotherShip(Game game){
        super(game,6, Race.GREEN);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(6,game), FloatRec.ship3);
        addUsualAction(new BonusCard(game),FloatRec.ship2);
        addComboAction(new BonusCard(game),FloatRec.ship1,game.combos.getGreen());
    }
}

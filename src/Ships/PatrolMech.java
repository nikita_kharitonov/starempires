package Ships;

import Actions.CollectDamage;
import Actions.CollectTrade;
import Actions.ShipUtilisation;
import mainP.*;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class PatrolMech extends Card {
    public PatrolMech(Game game){
        super(game,4, Race.RED);

    }

    @Override
    public void addActions() {
        Action trade = new CollectTrade(3,game);
        Action combat = new CollectDamage(5,game);
        trade.addOr(combat);
        addUsualAction(trade, FloatRec.ship3Left);
        addUsualAction(combat,FloatRec.ship3Right);
        addComboAction(new ShipUtilisation(game),FloatRec.ship2,game.combos.getRed());
    }
}

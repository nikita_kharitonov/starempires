package Ships;

import Actions.CollectTrade;
import Actions.CollectDamage;
import mainP.*;

/**
 * Created by artmmslv on 01.01.2017.
 */
public class Ram extends Card {
    public Ram(Game game){
        super(game,3,Race.GREEN);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(5,game), FloatRec.ship3);
        addComboAction(new CollectDamage(2,game), FloatRec.ship2,game.combos.getGreen());
        addUtilAction(new CollectTrade(3,game), FloatRec.ship1);
    }
}

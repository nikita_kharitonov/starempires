package Ships;

import Actions.CollectDamage;
import Actions.Discard1;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class RoyalRedoubt extends Card {
    public RoyalRedoubt(Game game){
        super(game,6, Race.YELLOW);
        thisIsBase(true,6);
    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(3,game), FloatRec.base2);
        addComboAction(new Discard1(game),FloatRec.base1,game.combos.getYellow());
    }
}

package Ships;

import Actions.CollectTrade;
import mainP.*;


/**
 * Created by artmmslv on 31.12.2016.
 */
public class Scout extends Card{
    public Scout(Game game){
        super(game,0,Race.NEUTRAL);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectTrade(1,game),FloatRec.ViperAndScout);
    }
}

package Ships;

import Actions.CollectDamage;
import Actions.CollectTrade;
import mainP.*;

/**
 * Created by Никиа on 07.01.2017.
 */
public class SpaceStation extends Card {
    public SpaceStation(Game game){
        super(game,4,Race.YELLOW);

        thisIsBase(true,4);
    }


    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(2,game), FloatRec.base2);
        addComboAction(new CollectDamage(2,game), FloatRec.base1Left,game.combos.getYellow());
        addUtilAction(new CollectTrade(4,game), FloatRec.base1Right);
    }
}

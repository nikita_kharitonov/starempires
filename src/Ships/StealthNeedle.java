package Ships;

import Actions.NeedleAction;
import mainP.*;


/**
 * Created by artmmslv on 18.01.2017.
 */
public class StealthNeedle extends Card implements ComboBonusGranter
{
    public StealthNeedle(Game game){
        super(game,4, Race.RED);

    }

    @Override
    public void coolDown() {
        getNeedleAction().coolDown();
    }

    @Override
    public void addActions() {
        addUsualAction(new NeedleAction(game,this),FloatRec.ship23);
    }

    public NeedleAction getNeedleAction(){
        Action onlyNeedlesAcrion = getAreas().get(0).getAction();
        if(onlyNeedlesAcrion instanceof NeedleAction){
            return  (NeedleAction)onlyNeedlesAcrion;
        }
        System.err.println("Needles Action is not NeedleAction");
        return new NeedleAction(game,this);
    }

    public Game getGame() {
        return game;
    }
    @Override
    public void addCombos(ObservableBoolean yellow, ObservableBoolean blue, ObservableBoolean green, ObservableBoolean red, ObservableBoolean bases) {
        NeedleAction action = getNeedleAction();
        if(action.hasStolen()){
            switch (action.getOrigin().getRace()){
                case YELLOW: yellow.set(true);
                case BLUE: blue.set(true);
                case GREEN:green.set(true);
                case RED: green.set(true);
            }
        }
        System.out.println("needle asked");
    }

}

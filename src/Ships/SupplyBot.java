package Ships;

import Actions.CollectDamage;
import Actions.CollectTrade;
import Actions.ShipUtilisation;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class SupplyBot extends Card {
    public SupplyBot(Game game){
        super(game,3, Race.RED);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectTrade(2,game), FloatRec.ship3);
        addUsualAction(new ShipUtilisation(game),FloatRec.ship2);
        addComboAction(new CollectDamage(2,game),FloatRec.ship1,game.combos.getRed());
    }
}

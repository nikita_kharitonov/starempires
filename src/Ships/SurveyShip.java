package Ships;

import Actions.BonusCard;
import Actions.CollectTrade;
import Actions.Discard1;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;


/**
 * Created by artmmslv on 18.01.2017.
 */
public class SurveyShip extends Card {
    public SurveyShip(Game game){
        super(game,3, Race.YELLOW);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectTrade(1,game), FloatRec.ship3);
        addUsualAction(new BonusCard(game),FloatRec.ship2);
        addUtilAction(new Discard1(game),FloatRec.ship1);
    }
}

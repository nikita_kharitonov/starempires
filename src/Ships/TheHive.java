package Ships;

import Actions.BonusCard;
import Actions.CollectDamage;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class TheHive extends Card {
    public TheHive (Game game){
        super(game,5, Race.GREEN);

        thisIsBase(false,5);
    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(3,game), FloatRec.base2);
        addComboAction(new BonusCard(game),FloatRec.base1,game.combos.getGreen());
    }
}

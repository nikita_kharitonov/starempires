package Ships;

import Actions.BonusCard;
import Actions.CollectDamage;
import Actions.Heal;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class TradeEscort extends Card {
    public TradeEscort(Game game){
        super(game, 5, Race.BLUE);

    }

    @Override
    public void addActions() {
        addUsualAction(new Heal(4,game), FloatRec.ship3Left);
        addUsualAction(new CollectDamage(4,game),FloatRec.ship3Right);
        addComboAction(new BonusCard(game),FloatRec.ship2,game.combos.getBlue());
    }
}

package Ships;

import Actions.CollectDamage;
import Actions.CollectTrade;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class TradePod extends Card {
    public TradePod(Game game){
        super(game,2, Race.GREEN);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectTrade(3,game), FloatRec.ship3);
        addComboAction(new CollectDamage(2,game),FloatRec.ship2,game.combos.getGreen());
    }
}

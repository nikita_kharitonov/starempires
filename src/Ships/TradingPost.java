package Ships;

import Actions.CollectDamage;
import Actions.CollectTrade;
import Actions.Heal;
import mainP.*;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class TradingPost extends Card {
    public TradingPost(Game game){
        super(game,3, Race.BLUE);
        thisIsBase(true,4);

    }

    @Override
    public void addActions() {
        Action Heal = new Heal(1,game);
        Action Trade = new CollectTrade(1,game);
        Heal.addOr(Trade);
        addUsualAction(Heal, FloatRec.base2Left);
        addUsualAction(Trade,FloatRec.base2Right);
        addUtilAction(new CollectDamage(3,game),FloatRec.base1);
    }
}

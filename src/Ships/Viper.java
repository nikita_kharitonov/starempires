package Ships;

import Actions.CollectDamage;
import mainP.*;

/**
 * Created by artmmslv on 31.12.2016.
 */
public class Viper extends Card{
    public Viper(Game game){
        super(game,0,Race.NEUTRAL);

    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(1,game), FloatRec.ViperAndScout);
    }
}

package Ships;

import Actions.CollectDamage;
import mainP.Card;
import mainP.FloatRec;
import mainP.Game;
import mainP.Race;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class WarWorld extends Card {
    public WarWorld(Game game){
        super(game,5, Race.YELLOW);

        thisIsBase(true,4);
    }

    @Override
    public void addActions() {
        addUsualAction(new CollectDamage(3,game), FloatRec.base2);
        addComboAction(new CollectDamage(4,game),FloatRec.base1,game.combos.getYellow());
    }
}

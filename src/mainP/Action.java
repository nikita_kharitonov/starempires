package mainP;

import java.util.ArrayList;

/**
 * Created by artmmslv on 31.12.2016.
 */
public abstract class Action implements Observable, Observer{
    //Абстракное действие.
    //Action может выполнять какое-то действие и управляет его доступностью.

    //Действие влияет на эту игру
    protected Game game;
    //Основной метод, отражающий суть действия. Абстрактный.
    protected abstract void realize();/*Должен вызываться только если все проверено, то есть изнутри этого класса*/

    //У действия есть изменяемое состояние, за ним надо следить
    //Если действие использовалось в этом ходу, его нельзя использовать.
    private boolean used = false;
    //Действие может иметь условия,типа наличия двух баз
    //Ограничения налагаются и на действия с требованием комбо. Cобрано комбо - действие доступно (permitted == true)
    //Если условие выполнено, то действие разрешено
    private ObservableBoolean permitted = new ObservableBoolean(true);
    //На действия карты, лежащей в магазине или в фолде, или чужую базу, нельзя нажимать.
    //То есть если карта лежит там, действия отключены.
    //Тогда нецелесообразно вообще их показывать.
    //Если карту выносят из активных, ее действия помечаются выключенными.
    private boolean disabled;


    //После ипользования ищет эту карту в руке или базах и отправляет ее в утиль
    //Если null, ничего не делает. Так что ненулевой эта карта будет только у действий утилизации. И содержать будет хозяина
    protected Card cardToDestroy = null;

    //После использования помечает и это действие использованным. Это для "или"
    protected Action or = null;
    public void addOr(Action alternative){
        alternative.or = this;
        this.or = alternative;
    }

    public Action(Game game){
        this.game = game;
    }
    public abstract Action same();

    public void setUsed(boolean newState){
        if(or!=null)or.markUsed(newState);//Помечаем альтернативу использованной
        used = newState;
        inform();
    }
    private void markUsed(boolean newState){//Способ избежать рекурсивного вызова в альтернативах
        used = newState;
        inform();
    }
    //set&get
    public void setDisabled(boolean newState){
        disabled = newState;
        inform();
    }
    public void setPermission(ObservableBoolean newPermission){
        permitted = newPermission;//вызывается в ComboAction
        permitted.addObserver(this);
        //не написано информ.потому что разрешение меняется только при создании карты
    }
    public boolean isActive(){return false;}//Сложное действие может быть активным. Простое - не может
    public boolean isUsed() {
        return used;
    }
    public boolean isRestricted(){return !permitted.get();}
    public boolean isAvailable(){
        //Действие может быть выполнено, если true
        return (!isRestricted()) && (!isUsed());
    }
    public boolean isDisabled(){
        return disabled;
    }
    public boolean isUtilisation(){return cardToDestroy != null;}
    public void setCardToDestroy(Card owner) {
        cardToDestroy = owner;
    }//пометить, что после выполнения надо уничтожить карту
    //Наблюдение за ограничением
    @Override
    public void refresh(){
        inform(); //Смысл в том, чтобы добавлять слежение за состоянием только один раз.
    }
                                       //Хотя переменных, за которыми следует следить, несколько
    //Наблюдение за состоянием действия
    ArrayList<Observer> observers = new ArrayList<>();
    @Override
    public void inform(){
        for (Observer observer :
                observers) {
            observer.refresh();
        }
    }
    @Override
    public void addObserver(Observer observer){
        observers.add(observer);
    }

    public boolean launch() {
        //Этот метод вызывается при клике по кнопочке. Если кнопку не должно быть видно, возвращает false
        if (disabled) return false;
        if(game.realizingActivatedAbility())return false;
        else if (isUsed()) game.log("Action is already Used");
        else if (isRestricted()) game.log("Action has a requirement");
        else {
            realize();
            setUsed(true);
            game.cardUtilisation(cardToDestroy);
        }
        return true;
    }


    public ArrayList<Observer> getObservers() {
        return observers;
    }
}

package mainP;

/**
 * Created by artmmslv on 16.01.2017.
 */
public class ActionArea{//Класс - пара объектов. Содержательного смысла не имеет
        //Одна зона - одно действие. Пока что //todo:может быть, сделать несколько действий на зону
        private Action action;
        private FloatRec rectangle;

        public ActionArea(Action action, FloatRec rectangle){
            this.action = action;
            this.rectangle = rectangle;
        }
        public Action getAction() {
            return action;
        }
        public FloatRec getRectangle() {
            return rectangle;
        }


}

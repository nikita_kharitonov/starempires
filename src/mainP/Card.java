package mainP;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by artmmslv on 31.12.2016.
 */
public abstract class Card {

    protected Game game;

    int cost;//Цена в магазине
    BufferedImage image;//Внешний вид карты
    private Race race;//Желтый красный синий зеленый или нейтральный



    //Все действия карты с указанем координат
    private ArrayList<ActionArea> areas = new ArrayList<>();

    //Имеет значение только у баз
    int hp = 0;
    DamageType damageType = DamageType.NONE;
    private boolean isBase = false;
    public Card(){
        addActions();
        setDefaultImage();
    }
    /* конструктор устанавливает цену, расу и картинку*/
    public Card(Game game, int cost, Race race){
        setGame(game);
        this.cost = cost;
        this.race = race;
        setDefaultImage();
        addActions();
    }
    protected void setGame(Game game) {
        this.game = game;
    }
    public int getCost() {
        return cost;
    }
    public void setCost(int cost) { this.cost = cost; }
    public int getHP() { return hp;}
    public void setHP(int hp) { this.hp = hp; }
    public boolean isBase() {
        return isBase;
    }
    public void thisIsBase(boolean isAvantpost, int hp){
        isBase = true;
        setDamageType(isAvantpost ? DamageType.BLACK : DamageType.WHITE);
        setHP(hp);
    }
    public DamageType getDamageType() {return damageType;}
    public void setDamageType(DamageType dt) { damageType = dt; }
    public BufferedImage getImage() {
        return image;
    }
    public void setImage(String path) {
        //Этот метод используется только в конструкторе конкретной карты
        //todo: может быть, сделатьего protected, abstract или вообще чертего знает каким
        try{
            File file = new File(path);
            image = ImageIO.read(file);
        }catch (Exception e){
            System.err.println("Image of " + getClass().getSimpleName() + " is not read. Path: "+ path);
        }
    }
    public void setBase(boolean base) {
        isBase = base;
    }
    public Race getRace() { return race; }
    public void setRace(Race rc) { race = rc; }
    public ArrayList<ActionArea> getAreas(){
        return areas;
    }
    public void setAreas(ArrayList<ActionArea> areas) {
        this.areas = areas;
    }
    //НАХ + Артём
    //Действие при нажатии на карту
    public void action (){
        game.cardAction(this);
        System.out.println(getClass().getSimpleName()+".action");
    }

    //НАХ
    //Сброс перезарядки у карты  конце хода
    //Вызывается из game, информирует о конце хода дейстия этой карты.
    public void coolDown(){
        for (int i = 0; i < areas.size(); i++)
            areas.get(i).getAction().setUsed(false);
    }
    public abstract void addActions();
    private void setDefaultImage(){
        String defaultPath = "images/GroupImagesPng/";
        String defaultImgType = ".png";
        String imgPath = defaultPath + this.getClass().getSimpleName() + defaultImgType;
        setImage(imgPath);
    }
    public void addUsualAction(Action action, FloatRec area){
        areas.add(new ActionArea(action,area));
    }
    public void addComboAction(Action action, FloatRec area, ObservableBoolean combo){
        action.setPermission(combo);
        areas.add(new ActionArea(action,area));
    }
    public void addUtilAction(Action action, FloatRec area){
        addUsualAction(action,area);
        action.setCardToDestroy(this);
    }
    public static void setUtilCard(List<ActionArea> list, Card card) {
        for (ActionArea area :
                list) {
            Action action = area.getAction();
            if (action.isUtilisation())
                action.setCardToDestroy(card);
        }
    }
    public void setDisabled(boolean newState){
        for (ActionArea area :
                areas) {
            area.getAction().setDisabled(newState);
        }
    }


}
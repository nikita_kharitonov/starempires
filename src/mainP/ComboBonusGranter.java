package mainP;

/**
 * Created by artmmslv on 19.01.2017.
 */
public interface ComboBonusGranter {
    void addCombos(ObservableBoolean yellow,
                   ObservableBoolean blue,
                   ObservableBoolean green,
                   ObservableBoolean red,
                   ObservableBoolean bases );


}

package mainP;

/**
 * Created by artmmslv on 16.01.2017.
 */
public class FloatRec {
    //todo: Можно добавить всяких разных дефолтных прямоугольников
    //todo: В каждой карте на действия добавляем либо новый прямоугольник либо дефолтный
    //Это готовые размеченные прямоугольнички.
    //Их можно добавлять на карты
    private final static float line0 = 0.99f;
    private final static float line1 = 0.865f;
    public final static float line2 = 0.775f;
    private final static float line3 = 0.685f;
    public final static float linb1 = 0.8f;
    public final static float linb2 = 0.6f;
    public final static float heitb = 0.2f;
    public final static float linel = 0.01f;
    private final static float linem = 0.50f;
    public final static float width = 0.98f;
    private final static float ahalf = 0.50f;
    public final static float heigt = 0.09f;
    private final static float heit2 = 0.20f;

    public final static FloatRec ViperAndScout = new FloatRec(0.3f,0.72f,0.4f,0.22f);
    public final static FloatRec Explorer = new FloatRec(0.3f,0.58f,0.4f,0.22f);
    public final static FloatRec ship1     = new FloatRec(linel,line1,width,heigt);
    public final static FloatRec ship2     = new FloatRec(linel,line2,width,heigt);
    public final static FloatRec ship3     = new FloatRec(linel,line3,width,heigt);
    public final static FloatRec ship1Right= new FloatRec(linem,line1,ahalf,heigt);
    public final static FloatRec ship1Left = new FloatRec(linel,line1,ahalf,heigt);
    public final static FloatRec ship12    = new FloatRec(linel,line2,width,heit2);
    public final static FloatRec ship3Left = new FloatRec(linel,line3,ahalf,heigt);
    public final static FloatRec ship2Left = new FloatRec(linel,line2,ahalf,heigt);
    public final static FloatRec ship2Right= new FloatRec(ahalf,line2,ahalf,heigt);
    public final static FloatRec ship3Right= new FloatRec(ahalf,line3,ahalf,heigt);
    public final static FloatRec base2     = new FloatRec(linel,linb2,width,heitb);
    public final static FloatRec base1     = new FloatRec(linel,linb1,width,heitb);
    public final static FloatRec base1Right= new FloatRec(ahalf,linb1,ahalf,heitb);
    public final static FloatRec base1Left = new FloatRec(linel,linb1,ahalf,heitb);
    public final static FloatRec base2Left = new FloatRec(linel,linb2,ahalf,heitb);
    public final static FloatRec base2Right= new FloatRec(ahalf,linb2,ahalf,heitb);
    public final static FloatRec ship23    = new FloatRec(linel,line3,width,heit2);

    //Тупо 4 флоата
    public float x;
    public float y;
    public float w;
    public float h;
    public FloatRec(float x, float y, float w, float h){
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }
    public FloatRec(FloatRec older){
        this.x = older.x;
        this.y = older.y;
        this.w = older.w;
        this.h = older.h;
    }
    //Содержит ли этот прямоугольник точку - аргумент метода?
    public boolean contains(float xf, float yf){
        if(xf < x) return false;
        if(yf < y) return false;
        if(xf > x + w) return false;
        if(yf > y + h) return false;
        return true;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getW() {
        return w;
    }

    public float getH() {
        return h;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setW(float w) {
        this.w = w;
    }

    public void setH(float h) {
        this.h = h;
    }

}

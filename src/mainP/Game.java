package mainP;

import Actions.Discard1;
import Ships.*;

import java.util.ArrayList;
import java.util.List;

public class Game {
    /*Класс, содержащий основные механики игры.
    * Активные спообности карт лежат в Actions, здесь - переносы карт в процессе хода и некоторые пассивные способности
    * */

    private ObservableInteger damage = new ObservableInteger();
    private ObservableInteger gold = new ObservableInteger();
    private ObservableInteger explorerCount = new ObservableInteger();//Они конечны. но инициализируются лениво.

    public TwoStepAction active;//Здесь находится активное в данный момент действие
    //Классы - помощники
    public Combos combos;//отдельное поле дляслежки за комбо
    public Current current = new Current();//Следит за текущим игроком

    private ObservableList<Card> deck;//карты, еще не вошедшие в игру.
    private ObservableList<Card> shop = new ObservableList<>(new ArrayList<Card>());//карты магазина, включая исследователя


    private List<Player> players;



    //НАХ
    //Конструктор Game
    public Game() {
        //todo: Сделать отдельную форму и прочие свистелки для настройки Game
        GameRules rules = new GameRules();//Игроки
        players = rules.getPlayers(this);
        current.init();
        //колода
        combos = new Combos();
        explorerCount.set(rules.startDeck_Explorers);
        deck = new ObservableList<>(rules.createDeck(this));
        //Доводка
        damage.set(0);
        gold.set(0);
        active = null;
        //Карты на стол
        createShop();
        current.getPlayer().toTable();
    }

    //НАХ
    //Некоторые геттеры
    public ObservableInteger getGold() { return gold; }
    public ObservableInteger getDamage() { return damage; }
    public List<Player> getPlayers() { return players; }
    public ObservableList<Card> getShop(){
        return shop;
    }
    public void setActive(TwoStepAction active) {
        this.active = active;
    }
    //НАХ
    //Переключение на следующего игрока
    public void nextPlayer() {
        //Общие вычисления
        int first = current.getNumber().get();
        int next = current.next();
        //Завершение хода
        for (Card card : current.getBases().list){
            card.coolDown();
            card.setDisabled(true);
        }
        players.get(first).handToFold();
        damage.set(0);
        gold.set(0);
        //Изменение текущего игрока
        current.pass(next);
        //Начало нового хода
        players.get(next).toTable();
        players.get(next).discard2.launch();//Механика желтых карт
    }

    //Методы перемещения карты по спискам
    public void newExplorerToShop(){
        int explorerPositionInShop = 0;// Magic, magic, magic,magic, maa-gic 5?
        explorerCount.deduct(1);
        Card card = new Explorer(this);
        card.setDisabled(true);
        shop.add(explorerPositionInShop,card);
    }
    //Сделано для Viper&Scout
    public void newCardToStock(Card card, ObservableList<Card> destination){
        card.setDisabled(true);
        destination.add(card);
    }
    public void deckToShop(int i,Card card){
        shop.add(i, card);
        card.setDisabled(true);
        deck.remove(card);
    }
    public void shopToFold(Card card){
        shop.remove(card);
        current.getFold().add(card);
    }
    public void shopToStock(Card card){
        int i = shop.indexOf(card);
        shop.remove(i);
        refillShop(i);
        current.getPlayer().getStock().add(0, card);
    }
    public void foldToStock(Card card){
        current.getFold().remove(card);
        current.getPlayer().getStock().add(card);
    }
    public void stockToTable(Card card){//Table= Bases+Hand
        current.getPlayer().getStock().remove(card);
        card.setDisabled(false);
        if(card.isBase()){
            current.getBases().add(card);
        }else{
            current.getHand().add(card);
        }
    }
    public void refillShop(int i){
        if ( i ==0 ){
            if(explorerCount.get() > 0)
                newExplorerToShop();
        }
        else if (deck.size() > 0) {
            Card fromDeck = deck.get(deck.size()-1);
            deckToShop(i,fromDeck);
        }
    }
    //Если применять его только к картам руки, то очистится только рука
    public void tableToFold(Card card){
        current.getBases().remove(card);
        current.getHand().remove(card);
        card.setDisabled(true);
        card.coolDown();
        current.getFold().add(card);
    }
    public void enemyBaseToFold(Card card){
        Player owner = players.get(whoseEnemyBase(card));
        owner.getBases().remove(card);
        owner.getFold().add(card);
        card.setDisabled(true);
    }
    public void cardUtilisation(Card card){
        current.getPlayer().getFold().remove(card);
        current.getPlayer().getHand().remove(card);
        current.getPlayer().getBases().remove(card);
    }
    public void dropShop(Card card){
        int i = shop.indexOf(card);
        shop.remove(i);
        refillShop(i);
    }

    //НАХ
    //Начальное заполнение магазина
    public void createShop() {
        for (int k = 0; k < 6; k++) {
            refillShop(k);
        }
    }



    //НАХ
    //Замена карты в магазине с перенесением в фолд игроку
    public void tryToBuy(Card crd)/* throws DeckEndException, FewGoldException*/ {
        if (crd.cost <= gold.get()) {
            //Денег достаточно - значит, покупаем
            int i = shop.indexOf(crd);
            shopToFold(crd);
            gold.deduct(crd.getCost());

            refillShop(i);
        }
    }


    public int whoseEnemyBase(Card card){
        for (int i = 0; i < players.size(); i++) {
            if (current.getPlayer() != players.get(i) && players.get(i).getBases().contains(card)) return i;
        }
        return -1;
    }
    //НАХ
    //Уничтожение базы противника с перемещением в сброс с помощью урона
    public void damageEnemyBase(Card crd){
        int i = whoseEnemyBase(crd);
        if(i>=0&& crd.getHP() <= damage.get()&&//И это не должна быть защищенная белая база
                (crd.getDamageType()==DamageType.BLACK||!players.get(i).hasAvantPost())) {
            damage.deduct(crd.getHP());
            enemyBaseToFold(crd);
            return;
        }
        log("Пытался атаковать базу. Оборзел, не иначе.");
    }

    //artmmslv
    //Нанесение единичного урона игроку
    public void dealDamage(Player player) {
        if (damage.get() <= 0){
            log("Нечем бить");
        }
        else if (current.isCurrent(player)){
            log("Не стоит бить себя");
        }
        else if (player.hasAvantPost()){
            log("Сначала надо пробить защиту");
        }
        else {//todo Подумать. Единичный урон якобы неудобен
            damage.deduct(1);
            player.getHP().deduct(1);
        }
    }

    public boolean realizingActivatedAbility(){
        return active != null;
    }

    //НАХ
    //Действие при клике на карту - понять откуда выполнить
    public void cardAction(Card crd) {
        if(active !=null){//Составное действие активно, обработаем его
            //Даже если это выбор игрока. (в этом случае просто отклоним)
            active.finish(crd);
            return;
        }
        if (whoseEnemyBase(crd)>=0){//это чужая база? бьем ее
            damageEnemyBase(crd);
            return;
        }
        if (shop.contains(crd)){//карту в магазине пробуем купить
            tryToBuy(crd);
            return;
        }
    }
    public boolean acceptPlayerClick(Player player){//Если нам надо выбирать игрока, примем клик. Иначе он не нужен.
        if( this.realizingActivatedAbility() && active.getClass().equals(Discard1.class)){
            Discard1 d1 = (Discard1) active;
            d1.finish(player);
            return true;
        }
        return false;
    }

    //Артём
    //todo Вывод лога
    public void log(String string){
        System.out.println("Log: " + string);
    }

    /*
    */
    public class Current {//переключатель хода
        //Несколько отслеживателей, удобных для разых целей во Viewer
        private ObservableInteger currentPlayerNumber = new ObservableInteger();
        private ObservableListDecorator<Card> currentFold, currentHand, currentBases;

        void init(){
            currentPlayerNumber.set(0);
            currentFold = new ObservableListDecorator<>(getPlayer().getFold());
            currentBases = new ObservableListDecorator<>(getPlayer().getBases());
            currentHand = new ObservableListDecorator<>(getPlayer().getHand());
        }
        int next(){
            int first = currentPlayerNumber.get();
            return first + 1 < players.size() ? first + 1 : 0;
        }
        void pass(int next){
            currentPlayerNumber.set(next);
            currentHand.setList(getPlayer().getHand());
            currentFold.setList(getPlayer().getFold());
            currentBases.setList(getPlayer().getBases());
        }
        //todo Заменить возвращаемое значения на ObservbleList
        public ObservableListDecorator<Card> getHand(){
            return currentHand;
        }
        public ObservableListDecorator<Card> getBases(){
            return currentBases;
        }
        public ObservableListDecorator<Card> getFold(){
            return currentFold;
        }
        boolean isCurrent(Player p){
            return getPlayer() == p;
        }
        public ObservableInteger getNumber(){
            return currentPlayerNumber;
        }
        public Player getPlayer() {
            return players.get(currentPlayerNumber.get());
        }
    }

    public class Combos implements Observer{
        //класс (отдельный набор методов и полей)  для слежения за комбо.
        ObservableBoolean green = new ObservableBoolean(false);
        ObservableBoolean red = new ObservableBoolean(false);
        ObservableBoolean blue = new ObservableBoolean(false);
        ObservableBoolean yellow = new ObservableBoolean(false);
        ObservableBoolean bases = new ObservableBoolean(false);

        public Combos(){
            current.getHand().addObserver(this);
            current.getBases().addObserver(this);
            refresh();
        }

        @Override
        public void refresh(){
            //Этот метод пересчитывает карты на столе и устанавливает, включены ли комбо
            int countYellow = 0;
            int countRed = 0;
            int countBlue = 0;
            int countGreen = 0;

            for (int i = 0; i < current.getHand().size(); i++) {
                Race race = current.getHand().get(i).getRace();
                if(race == Race.YELLOW)countYellow++;
                if(race == Race.RED)countRed++;
                if(race == Race.BLUE)countBlue++;
                if(race == Race.GREEN)countGreen++;
            }
            for (int i = 0; i < current.getBases().size(); i++) {
                Race race = current.getBases().get(i).getRace();
                if(race == Race.YELLOW)countYellow++;
                if(race == Race.RED)countRed++;
                if(race == Race.BLUE)countBlue++;
                if(race == Race.GREEN)countGreen++;
            }

            //set вызывает информ, только если значение изменилось
            yellow.set(countYellow >= 2);
            red.set(countRed >= 2);
            blue.set(countBlue >= 2);
            green.set(countGreen >= 2);
            bases.set(current.getBases().size() >= 2);

            for (Card base :
                    current.getBases().list) {
                if(base instanceof ComboBonusGranter)
                    ((ComboBonusGranter) base).addCombos(yellow,blue,green,red,bases);
            }
            for (Card ship :
                    current.getHand().list) {
                if(ship instanceof ComboBonusGranter)
                    ((ComboBonusGranter) ship).addCombos(yellow,blue,green,red,bases);
            }
        }

        public void addObserver(Observer observer, Race race){
            if(race == Race.GREEN)green.addObserver(observer);
            if(race == Race.BLUE)blue.addObserver(observer);
            if(race == Race.RED)red.addObserver(observer);
            if(race == Race.YELLOW)yellow.addObserver(observer);
        }

        public ObservableBoolean getGreen() {
            return green;
        }

        public ObservableBoolean getRed() {
            return red;
        }

        public ObservableBoolean getYellow() {
            return yellow;
        }

        public ObservableBoolean getBlue() {
            return blue;
        }

        public ObservableBoolean getBases(){
            return bases;
        }
    }
}

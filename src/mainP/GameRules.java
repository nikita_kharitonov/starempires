package mainP;

import Ships.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by artmmslv on 09.01.2017.
 */
public class GameRules {
    public int default_Authority;
    public int max_Authority;
    public int startStock_Vipers;
    public int startStock_Scouts;
    public int startDeck_Explorers;
//todo нужно больше правил! и настраивать!
    public GameRules(){
        default_Authority = 50;
        max_Authority = 50;
        startStock_Scouts = 8;
        startStock_Vipers = 2;
        startDeck_Explorers = 10;
    }
    public List<Player> getPlayers(Game game){
        List list = new ArrayList();
        list.add(constructPlayer("Бармалей", game));
        list.add(constructPlayer("Пересвет", game));
        return list;
    }
    protected Player constructPlayer(String name, Game game){
        Player player = new Player(name,game);
        player.getHP().set(50);
        fillStock(player);
        return player;
    }
    public void fillStock(Player player){
        Game game = player.game;
        ObservableList<Card> stock = player.getStock();
        for (int i = 0; i < startStock_Vipers; i++)
            game.newCardToStock(new Viper(game),stock);
        for (int i = 0; i < startStock_Scouts; i++)
            game.newCardToStock(new Scout(game),stock);
        Collections.shuffle(stock);
    }
    public List<Card> createDeck(Game game) {
        ArrayList<Card> deck = new ArrayList<>();
        //*Для тестирования, содержимое want будет помещено в конце списка и появится в игре быстро*/
        ArrayList<Card> want = new ArrayList<>();
        deck.add(new BattleBlob(game));

        deck.add(new BlobFighter(game));
        deck.add(new BlobFighter(game));
        deck.add(new BlobFighter(game));

        deck.add(new Corvette(game));
        deck.add(new Corvette(game));

        deck.add(new Dreadnaught(game));

        deck.add(new EmbassyYacht(game));
        deck.add(new EmbassyYacht(game));


        deck.add(new Ram(game));
        deck.add(new Ram(game));

        deck.add(new BlobDestroyer(game));
        deck.add(new BlobDestroyer(game));

        deck.add(new BattleStation(game));
        deck.add(new BattleStation(game));

        deck.add(new SpaceStation(game));
        deck.add(new SpaceStation(game));

        deck.add(new CommandShip(game));

        deck.add(new BattleMech(game));

        deck.add(new BattlePod(game));
        deck.add(new BattlePod(game));

        deck.add(new BlobCarrier(game));

        deck.add(new BlobWheel(game));
        deck.add(new BlobWheel(game));
        deck.add(new BlobWheel(game));

        deck.add(new Cutter(game));
        deck.add(new Cutter(game));
        deck.add(new Cutter(game));

        deck.add(new FederationShuttle(game));
        deck.add(new FederationShuttle(game));
        deck.add(new FederationShuttle(game));

        deck.add(new Flagship(game));

        deck.add(new Junkyard(game));

        deck.add(new MissileBot(game));
        deck.add(new MissileBot(game));
        deck.add(new MissileBot(game));

        deck.add(new MissileMech(game));

        deck.add(new MotherShip(game));

        deck.add(new SupplyBot(game));
        deck.add(new SupplyBot(game));
        deck.add(new SupplyBot(game));

        deck.add(new TheHive(game));

        deck.add(new TradeBot(game));
        deck.add(new TradeBot(game));
        deck.add(new TradeBot(game));

        deck.add(new TradeEscort(game));

        deck.add(new TradePod(game));
        deck.add(new TradePod(game));
        deck.add(new TradePod(game));

        deck.add(new WarWorld(game));

        deck.add(new BarterWorld(game));
        deck.add(new BarterWorld(game));

        deck.add(new DefenseCenter(game));

        deck.add(new PatrolMech(game));
        deck.add(new PatrolMech(game));

        deck.add(new TradingPost(game));
        deck.add(new TradingPost(game));

        deck.add(new ImperialFighter(game));
        deck.add(new ImperialFighter(game));
        deck.add(new ImperialFighter(game));

        deck.add(new ImperialFrigate(game));
        deck.add(new ImperialFrigate(game));
        deck.add(new ImperialFrigate(game));

        deck.add(new RoyalRedoubt(game));

        want.add(new StealthNeedle(game));

        Collections.shuffle(deck);
        Collections.shuffle(want);
        deck.addAll(want);
        return deck;
    }

}

package mainP;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by artmmslv on 04.01.2017.
 */
public interface Observable {
    //todo:Создать ObservableInteger
    void inform();//Метод для сообщения всем подписчикам об изменении содержимого
    void addObserver(Observer observer);//Метод для получения подписки
    /* Рекомендуемая реалиация
        Отслеживаемые переменные сделать приватными

        ArrayList<Observer> observers = new ArrayList<>();
        @Override
        public void inform(){
            for(Observer observer: observers){
                observer.refresh();
            }
        }
        @Override
        public void addObserver(Observer observer){
            observers.add(observer);
        }

     */
}

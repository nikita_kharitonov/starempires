package mainP;

import java.util.ArrayList;

/**
 * Created by artmmslv on 16.01.2017.
 */
public class ObservableBoolean implements Observable {
    private boolean value = false;
    ArrayList<Observer> observers = new ArrayList<>();
    public ObservableBoolean(boolean value){
        this.value = value;
    }

    public boolean getValue(){
        return value;
    }

    public void setValue(boolean newValue){
        set(newValue);
    }

    @Override
    public void inform(){
        for(Observer observer: observers){
            observer.refresh();
        }
    }
    @Override
    public void addObserver(Observer observer){
        observers.add(observer);
    }

    public boolean get() {
        return value;
    }
    public void set(boolean newValue){
        if(newValue!=value){
            value = newValue;
            //inform банально может долго делаться, учитывая ненужные repaint
            inform();
        }
    }
}

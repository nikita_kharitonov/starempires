package mainP;

import java.util.ArrayList;

/**
 * Created by artmmslv on 04.01.2017.
 */
public class ObservableInteger   implements Observable{
    private int value;
    private ArrayList<Observer> observers = new ArrayList<>();
    public ObservableInteger(){
        this(0);
    }
    public ObservableInteger(int i)
    {
        value = i;
    }
    public int get(){return value;}
    public void set(int i){
        value = i;
        inform();
    }
    public void add(int value){
        this.value+=value;
        inform();
    }
    public void deduct(int value){
        this.value-=value;
        inform();
    }

   @Override
    public void inform() {
       for (Observer observer : observers) {
           observer.refresh();
       }
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }


    @Override
    public String toString(){
        return Integer.toString(value);
    }

}

package mainP;

import java.util.*;

/**
 * Created by artmmslv on 04.01.2017*/
public class ObservableList<T> implements Observable, List{
    public List<T> list;
    public ArrayList<Observer> observers;
    //todo: есть косяки с дженериками
    //Реализует паттерн обертка для List
    public ObservableList(List list){
        this.list = (List<T>)list;
        observers = new ArrayList();
    }
    @Override
    public void inform() {
        for (Observer o :
                observers) {
            o.refresh();
        }
    }
    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return list.contains(o);
    }
    public boolean contains(Class classy){
        for (T obj :
                list) {//Содержит ли список объект конкретно этого класса?
            if(obj.getClass().equals(classy))return true;
        };
        return false;
    }

    @Override
    public Iterator iterator() {
        return list.iterator();
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public boolean add(Object o) {
        boolean success = list.add((T)o);
        inform();
        return success;
    }

    @Override
    public boolean remove(Object o) {
        boolean success = list.remove(o);
        inform();
        return success;
    }

    @Override
    public boolean addAll(Collection c) {
        boolean success = list.addAll(c);
        inform();
        return success;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        boolean success =  list.addAll(index,c);
        inform();
        return success;
    }

    @Override
    public void clear() {
        list.clear();
        inform();
    }

    @Override
    public T get(int index) {
        return list.get(index);
    }

    @Override
    public Object set(int index, Object element) {
        //todo: cast is not universal. fix
        Object obj = list.set(index,(T)element);
        inform();
        return obj;
    }

    @Override
    public void add(int index, Object element) {
        list.add(index,(T)element);
        inform();
    }

    @Override
    public T remove(int index) {
        T success = list.remove(index);
        inform();
        return success;
    }

    @Override
    public int indexOf(Object o) {
        return list.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return list.lastIndexOf(o);
    }

    @Override
    public ListIterator listIterator() {
        return list.listIterator();
    }

    @Override
    public ListIterator listIterator(int index) {
        return list.listIterator(index);
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return list.subList(fromIndex,toIndex);
    }

    @Override
    public boolean retainAll(Collection c) {
        boolean success =  list.retainAll(c);
        inform();
        return success;
    }

    @Override
    public boolean removeAll(Collection c) {
        boolean success =  list.removeAll(c);
        inform();
        return success;
    }

    @Override
    public boolean containsAll(Collection c) {
        return list.containsAll(c);
    }

    @Override
    public Object[] toArray(Object[] a) {
        return list.toArray(a);
    }
}

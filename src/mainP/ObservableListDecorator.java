package mainP;


/**
 * Created by artmmslv on 09.01.2017.
 */
public class ObservableListDecorator<T> extends ObservableList<T> implements Observer{
    //Добавляет к ObservableList функцию полной смены листа
    public ObservableListDecorator(ObservableList<T> list){
        super(list);
        list.addObserver(this);
    }
    public void setList(ObservableList<T> list){
        this.list = list;
        list.addObserver(this);
        refresh();
    }
    public void refresh(){
        inform();
    }
}

package mainP;

/**
 * Created by artmmslv on 04.01.2017.
 */
public interface Observer {//Интерфейс обсервер дает возможность получать обновления о том, на что подписался
    void refresh();
}

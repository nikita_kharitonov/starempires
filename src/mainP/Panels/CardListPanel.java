package mainP.Panels;

import Ships.StealthNeedle;
import mainP.Card;
import mainP.ObservableList;
import mainP.Observer;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by artmmslv on 04.01.2017.
 */
public class CardListPanel extends MyPanel implements Observer{
    ObservableList<Card> cardList;
    Dimension baseTeplate, shipTemplate;
    public CardListPanel(ObservableList<Card> cardList, Rectangle panelTemplate, Dimension baseTemplate, Dimension shipTemplate){
        super(panelTemplate);
        FlowLayout layout = new FlowLayout();
        layout.setVgap(0);
        layout.setHgap(20);
        setLayout(layout);
        this.cardList = cardList;
        this.baseTeplate = baseTemplate;
        this.shipTemplate = shipTemplate;
        cardList.addObserver(this);
        refresh();
    }
    public void refresh(){
            removeAll();
            for (Card card : cardList.list) {
                    CardPanel cp;
                    if(card.isBase())
                        cp = new CardPanel(card,baseTeplate);
                    else
                    {
                        if(card instanceof StealthNeedle){
                            cp = new StealthNeedlePanel((StealthNeedle)card,shipTemplate);
                        }else
                        cp = new CardPanel(card,shipTemplate);
                    }
                    add(cp);
                    cp.setVisible(true);
                }
            //repaint? revalidate
            setVisible(!isVisible());
            setVisible(!isVisible());

        }



    @Override
    public void paint(Graphics g) {
        if(image == null){
        g.setColor(color);
        g.fillRect(0,0,getWidth(), getHeight());
        }else {
            PhotoBank.drawImgInRectangle(g,image,getSize());
        }
        PhotoBank.repaintComponents(this);
    }

}

package mainP.Panels;

import mainP.*;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by artmmslv on 02.01.2017.
 */
public class CardPanel extends MyButton implements Observer{
    BufferedImage img;
    protected Card card;
    public CardPanel(Card c, Dimension template){
        super(new Rectangle(new Point(0,0),template), c.getImage());
        this.card = c;
        this.img = card.getImage();
        setLayout(null);
        listenToActions();
        refresh();
    }
    protected void listenToActions(){
        for (ActionArea aa :
                getAreas()) {
            Action action = aa.getAction();
            boolean hasNOT = !aa.getAction().getObservers().contains(this);
            if (aa != null && hasNOT) {
                aa.getAction().addObserver(this);
            }
        }
    }
    public void refresh(){
        repaint();
    }
    protected void drawActionAreas(Graphics g, ArrayList<ActionArea> areas){
        for (int i = 0; i < areas.size(); i++) {
            ActionArea aa = areas.get(i);
            if(aa!=null){
                Rectangle rectangle = areaRectangle(aa.getRectangle());
                g.setColor(stateIndicator(aa.getAction()));
                g.fillRect(rectangle.x,rectangle.y,rectangle.width,rectangle.height);
            }
        }
    }
    public Color stateIndicator(Action action){
        if(action.isActive())return MyLabel.shaderActive;
        if(action.isDisabled())return MyLabel.transparent;
        if(action.isAvailable())return MyLabel.shaderEnabled;
        if(action.isRestricted())return MyLabel.shaderDisabled;
        if(action.isUsed())return MyLabel.shaderUsed;
        return null;
    }
    public Rectangle areaRectangle(FloatRec d){
        int W = getWidth();
        int H = getHeight();
        int x = (int)(d.getX()*W);
        int y = (int)(d.getY()*H);
        int w = (int)(d.getW()*W);
        int h = (int)(d.getH()*H);
        return new Rectangle(x,y,w,h);
    }
    public Dimension getSize(){
        return new Dimension(getWidth(),getHeight());
    }
    public ArrayList<ActionArea> getAreas(){
        return card.getAreas();
    }
    @Override
    public  void updateSize(){
        //NOT using setBounds as CardPanel should be layouted automatically
        setPreferredSize(new Dimension(template.width,template.height));
    }
    @Override
    public void button(){
        //Overriden void MyAddMouseListener doesn`t call button()
        System.err.println("CardPanel.button()");
    }
    @Override
    public void MYaddMouseListener(){
        MouseListener m1 = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                    float xf = (float) (e.getX()) / getWidth();
                    float yf = (float) (e.getY()) / getHeight();
                    for (ActionArea aa :
                            getAreas()) {
                        if (aa.getRectangle().contains(xf, yf)) {//Проверяем. что попали мышкой в прямоугольник
                            //вызываем метод обработки клика у ActionArea
                            //И принимаем результат
                            boolean accepted = aa.getAction().launch();
                            //Если клик принят - все, закончили. Если не принят - смотрим на другие прямоугольники
                            if (accepted)
                                return;
                        }
                    }
                card.action();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        };
        addMouseListener(m1);
    }
    @Override
    public void paint(Graphics g){
        super.paint(g);
        drawActionAreas(g, getAreas());
    }
}

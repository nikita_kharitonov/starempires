package mainP.Panels;

import mainP.ObservableInteger;
import mainP.Observer;

import java.awt.*;

/**
 * Created by artmmslv on 09.01.2017.
 */
public abstract class HealthPanel extends MyButton implements Observer {
    ObservableInteger health;
    ObservableInteger combat;
    public HealthPanel(ObservableInteger health, ObservableInteger combat, Rectangle rectangle, String imgPath){
        super(rectangle,imgPath);
        this.health = health;
        this.combat = combat;
        health.addObserver(this);
        refresh();
    }
    @Override
    public void refresh(){repaint();}
    public void paint(Graphics g){
        super.paint(g);
        g.setColor(MyLabel.black);
        g.setFont(MyLabel.font);
        g.drawString(health.toString(),xLabel(),yLabel());
    }
    private int xLabel(){
        return getWidth()/2-11;
    }
    private int yLabel(){
        return getHeight()/2-5;
    }
}
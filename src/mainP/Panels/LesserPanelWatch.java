package mainP.Panels;

import mainP.Observer;

import javax.swing.*;

/**
 * Created by artmmslv on 02.01.2017.
 */
public class LesserPanelWatch {
//Lesser panel  это свободная зона, на которой находится одна из других панелек
    //Этот класс следит за состоянием этой зоны
    private static final int bases = 0;
    private static final int shop = -1;
    private static final int fold = -2;
    private int current;
    private JScrollPane bigBasesPanel,bigShopPanel,bigFoldPanel;
    public LesserPanelWatch(JScrollPane bigBasesPanel, JScrollPane bigShopPanel, JScrollPane bigFoldPanel){
        this.bigBasesPanel = bigBasesPanel;
        this.bigShopPanel = bigShopPanel;
        this.bigFoldPanel = bigFoldPanel;
        openBases();
    }
    public void shop(){
        if(current == shop){
            bigShopPanel.setVisible(false);
            bigBasesPanel.setVisible(true);
            current = bases;
        }
        else
        if(current == bases){
            bigBasesPanel.setVisible(false);
            bigShopPanel.setVisible(true);
            current = shop;
        }
        else
            if(current == fold){
                bigFoldPanel.setVisible(false);
                bigShopPanel.setVisible(true);
                current = shop;
            }
    }
    public void fold(){
        if(current == shop){
            bigShopPanel.setVisible(false);
            bigFoldPanel.setVisible(true);
            current = fold;
        }
        else
        if(current == bases){
            bigBasesPanel.setVisible(false);
            bigFoldPanel.setVisible(true);
            current = fold;
        }
        else
        if(current == fold){
            bigFoldPanel.setVisible(false);
            bigBasesPanel.setVisible(true);
            current = bases;
        }
    }
    public void openBases(){
        current = bases;
        bigFoldPanel.setVisible(false);
        bigShopPanel.setVisible(false);
        bigBasesPanel.setVisible(true);
    }
    public void openShop(){
        current = shop;
        bigFoldPanel.setVisible(false);
        bigBasesPanel.setVisible(false);
        bigShopPanel.setVisible(true);
    }
    public void openFold(){
        current = fold;
        bigBasesPanel.setVisible(false);
        bigShopPanel.setVisible(false);
        bigFoldPanel.setVisible(true);
    }

}

package mainP.Panels;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by artmmslv on 02.01.2017.
 */
public abstract class MyButton extends MyPanel {
    public MyButton(Rectangle template, String imgPath){
        this(template,PhotoBank.loadImg(imgPath));
    }
    public MyButton(Rectangle template, BufferedImage img){
        super(template);
        this.image = img;
        MYaddMouseListener();
    }
    public void MYaddMouseListener(){
        MouseListener ml = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                button();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        };
        addMouseListener(ml);
    }

    public abstract void button();
}

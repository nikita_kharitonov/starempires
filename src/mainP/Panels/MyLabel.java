package mainP.Panels;

import javax.swing.*;
import java.awt.*;

/**
 * Created by artmmslv on 02.01.2017.
 */
public class MyLabel extends JLabel {
    public final static Color shop = new Color(220,170,50);
    public final static Color currentPlayer = shop;
    public final static Color otherPlayer = new Color(40,40,130);
    public final static Color gold = shop;
    public final static Color black = Color.black;
    public final static Color red = new Color(130,15,15);
    public final static Font font = new Font("Verdana", Font.BOLD, 15);
    public final static Color shaderEnabled = new Color(0,0,190,80);
    public final static Color shaderUsed = new Color(190,40,40,150);
    public final static Color shaderActive = new Color(40,190,40,150);
    public final static Color shaderDisabled = new Color(100,100,100,150);
    public final static Color transparent = new Color(0,0,0,0);
    public MyLabel(String string, Color color){
        super(string);
        setFont(new Font("Verdana", Font.BOLD,15));
        setForeground(color);
    }


}

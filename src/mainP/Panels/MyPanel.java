package mainP.Panels;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

/**
 * Created by artmmslv on 02.01.2017.
 */
public class MyPanel extends JPanel{
    Rectangle template;
    BufferedImage image;
    Color color = PhotoBank.randomColor();
    public MyPanel(Rectangle template) {
        super();
        this.template = template;
        updateSize();
        setVisible(true);
    }
    public MyPanel(Rectangle template, BufferedImage image){
        this(template);
        this.image = image;
    }
    public MyPanel(Rectangle template, String imagePath){
        this(template);
        setImage(imagePath);
    }
    public void updateSize(){
        setBounds(template);
    }
    @Override
    public void paint(Graphics g){
        try{
            PhotoBank.drawStretchedImage(g,image,template);
        }catch (Exception e){
            try {
                fill(g);
            }catch (Exception e2){
                System.err.println("Panel failed to render");
            }
        }
    }

    private void fill(Graphics g)throws Exception{
        g.setColor(color);
        g.fillRect(0, 0, getWidth(), getHeight());
    }
    public void setImage(String path){
        image = PhotoBank.loadImg(path);
    }
    public void setImage(BufferedImage img){
        image = img;
    }
}

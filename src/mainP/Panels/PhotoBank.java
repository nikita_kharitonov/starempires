package mainP.Panels;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by artmmslv on 05.01.2017.
 */

public class PhotoBank {
    public static BufferedImage photo(){
        return loadImg("images/BattlePod.jpg");
    }
    public static void drawImgInRectangle(Graphics g, BufferedImage img, Dimension dimension){
        Rectangle rectangle = new Rectangle(new Point(0,0),dimension);
        drawImgInRectangle(g,img,rectangle);
    }
    public static void drawImgInRectangle(Graphics g, BufferedImage img, Rectangle rectangle){
        try{
            double scale1 = (rectangle.getWidth())/img.getWidth();
            double scale2 = (rectangle.getHeight())/img.getHeight();
            BufferedImage scaledImg = new BufferedImage(rectangle.width,rectangle.height,BufferedImage.TYPE_3BYTE_BGR);
            Graphics2D g2D = (Graphics2D)scaledImg.getGraphics();
            g2D.drawImage(img, AffineTransform.getScaleInstance(scale1,scale2), null);
            g.drawImage(scaledImg,rectangle.x,rectangle.y,null);
        }catch (Exception e){
            System.err.println("PB drawImg failed");
        }
    }
    public static Color randomColor(){
        return  new Color((int)(256*Math.random()),(int)(256*Math.random()),(int)(256*Math.random()));
    }
    public static BufferedImage loadImg(String path){
        try{
            return ImageIO.read(new File(path));
        }catch (Exception e){
            System.err.println("PB Failed to read image " + path);
            return  null;}
    }

    public static void repaintComponents(Container container){
        Component[] components = container.getComponents();
        for (int i = 0; i < components.length; i++) {
            components[i].repaint();
        }
    }
    public static BufferedImage photoShop(BufferedImage origin, BufferedImage over, float cut){
        BufferedImage modifiedImage = new BufferedImage(
                origin.getWidth(),
                origin.getHeight(),
                BufferedImage.TYPE_INT_RGB                );
        Graphics gI = modifiedImage.getGraphics();
        gI.drawImage(origin,0,0,null);
        gI.drawImage(over,
                0,/*dx1*/
                origin.getHeight() - (int)(cut*origin.getHeight()),/*dy1*/
                origin.getWidth(),/*dx2*/
                origin.getHeight(),/*dy2*/
                0,/*sx1*/
                over.getHeight() - (int)(cut*over.getHeight()),/*sy1*/
                over.getWidth(),/*dw1*/
                over.getHeight(),
                null);
        return modifiedImage;
    }
    protected static void drawStretchedImage(Graphics g, BufferedImage img, Rectangle rectangle){
        Graphics2D g2D = (Graphics2D) g;
        double scale1 = ((double)rectangle.getWidth())/img.getWidth();
        double scale2 = ((double)rectangle.getHeight())/img.getHeight();
        g2D.drawImage(img, AffineTransform.getScaleInstance(scale1,scale2), null);
    }


}

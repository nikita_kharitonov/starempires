package mainP.Panels;

import mainP.Observer;
import mainP.Player;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by artmmslv on 02.01.2017.
 */
public abstract class PlayerPanel extends MyButton implements Observer {
    Player p;
    BufferedImage texture = null;
    public PlayerPanel(Player player, Rectangle template) {
        super(template, PhotoBank.photo());
        p = player;
        p.getHP().addObserver(this);
        p.getFold().addObserver(this);
        p.getStock().addObserver(this);
        refresh();
    }
    @Override
    public void paint(Graphics g){
        g.setColor(color);
        g.fillRect(0,0,getWidth(),getHeight());
        g.setFont(MyLabel.font);
        g.setColor(p.isCurrent() ? MyLabel.currentPlayer : MyLabel.otherPlayer);
        g.drawString(p.getName(),nameX(), y());
        g.setColor(MyLabel.otherPlayer);
        g.drawString("Fold: " + p.getFold().size(), foldX(), y());
        g.drawString("Stock: " + p.getStock().size(), stockX(), y());
        g.drawString("Health: " + p.getHP().toString(), healthX(), y());

    }
    @Override
    public void refresh(){
        repaint();
    }
    private int nameX(){
        return 20;
    }
    private int y(){
        return getHeight()/2 - 7;
    }
    private int foldX(){
        return 130;
    }
    private int stockX(){
        return 200;
    }
    private int healthX(){
        return 290;
    }
    private int handX(){
        return 330;
    }
}

package mainP.Panels;

import mainP.ObservableInteger;
import mainP.Observer;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by artmmslv on 02.01.2017.
 */
public class ResourcePanel extends MyPanel implements Observer {
    ObservableInteger gold, damage;
    public ResourcePanel(ObservableInteger gold, ObservableInteger damage, Rectangle template){
        super(template);
        this.gold = gold;
        this.damage = damage;
        gold.addObserver(this);
        damage.addObserver(this);
        refresh();
    }
    @Override
    public void refresh() {
        repaint();
    }
    @Override
    public void paint(Graphics g){
        super.paint(g);
        g.setColor(Color.black);
        g.setFont(MyLabel.font);
        BufferedImage gold = PhotoBank.loadImg("images/Interface/GoldIcon.png");
        BufferedImage damage = PhotoBank.loadImg("images/Interface/CombatIcon.png");
        PhotoBank.drawImgInRectangle(g,gold,tradeRectangle());
        PhotoBank.drawImgInRectangle(g,damage,combatRectangle());
        g.drawString(goldString(), tradePoint().x, tradePoint().y);
        g.drawString(combatString(), combatPoint().x, combatPoint().y);
    }

    private String goldString(){
        return Integer.toString(gold.get());
    }
    private String combatString(){
        return Integer.toString(damage.get());
    }
    private Rectangle tradeRectangle(){
        int x = (int)(getWidth()*0.2f);
        int y = (int)(getHeight()*0.2f);
        int w = (int)(getWidth()*0.6f);
        int h = (int)(getHeight()*0.2f);
        return new Rectangle(x,y,w,h);
    }
    private Rectangle combatRectangle(){
        int x = (int)(getWidth()*0.2f);
        int y = (int)(getHeight()*0.4f);
        int w = (int)(getWidth()*0.6f);
        int h = (int)(getHeight()*0.2f);
        return new Rectangle(x,y,w,h);
    }
    private Point tradePoint(){
        int x = (int)(getWidth()*0.4f);
        int y = (int)(getHeight()*0.31f);
        return new Point(x,y);
    }
    private Point combatPoint(){
        int x = (int)(getWidth()*0.4f);
        int y = (int)(getHeight()*0.515f);
        return new Point(x,y);
    }
}


package mainP.Panels;

import Ships.StealthNeedle;
import mainP.ActionArea;
import mainP.Card;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by artmmslv on 18.01.2017.
 */
public class StealthNeedlePanel extends CardPanel{
    public static float cutHeight = 364f/1024;
    public StealthNeedlePanel(StealthNeedle card, Dimension template){
        super(card,template);
    }
    @Override
    public void paint(Graphics g){
        BufferedImage needleImg = card.getImage();
        if(hasStolen()){
            //todo: Исправить баг с рисовкой, когда картинки разного размера
            BufferedImage copyImage = getOrigin().getImage();
            BufferedImage modifiedImage = PhotoBank.photoShop(needleImg,copyImage,cutHeight);
            PhotoBank.drawStretchedImage(g,modifiedImage,template);
        }else
            PhotoBank.drawStretchedImage(g,needleImg,template);
        drawActionAreas(g, getAreas());
    }

    @Override
    public ArrayList<ActionArea> getAreas(){
        return hasStolen() ? ((StealthNeedle)card).getNeedleAction().getStolenAreas() : card.getAreas();
    }
    protected Card getOrigin(){
        return ((StealthNeedle) card).getNeedleAction().getOrigin();
    }
    public boolean hasStolen(){
        //needle не инициализируется при вызове super-конструктора
        return ((StealthNeedle)card).getNeedleAction().hasStolen();
    }
    @Override
    public void refresh(){
        listenToActions();
        repaint();
    }
    protected int getCutY(){
        return (int)cutHeight;
    }
    protected int getCutHeight(){
        return (int)(cutHeight*getHeight());
    }

}

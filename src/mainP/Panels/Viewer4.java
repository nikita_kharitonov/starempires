package mainP.Panels;

import javafx.scene.control.ScrollBar;
import mainP.Game;
import mainP.Player;

import javax.swing.*;
import java.awt.*;

/**
 * Created by artmmslv on 06.01.2017.
 */

public class Viewer4 {
    private final double cardPhi = (1+Math.sqrt(5))/2;
    private final static String turnOffPath = "images/Interface/OffButton.png";
    private final static String shopPath = "images/Interface/ShopButton.png";
    private final static String bigShopPath = "images/Interface/ShopPanel.png";
    private final static String foldPath = "images/Interface/FoldButton.png";
    private final static String bigFoldPath = "images/Interface/FoldPanel.png";
    private final static String bigBasesPath = "images/Interface/BasesPanel.png";
    private final static String greaterPath = "images/Interface/Hand.png";
    private final static String endPath = "images/Interface/EndButton.png";
    private final static String healthPath = "images/Interface/HealthPanel.png";
    private JFrame jFrame;
    private JLayeredPane jLayeredPanel;
    private MyButton turnOffButton, shopPanel, foldPanel, endPanel;
    private CardListPanel bigShopPanel, bigFoldPanel, bigBasesPanel;
    private JScrollPane shopPane,foldPane,basesPane;
    private CardListPanel greaterPanel;
    private ResourcePanel resourcePanel;
    private MyPanel logPanel;
    private LesserPanelWatch lesserPanelWatch;
    private PlayerPanel[] playerPanels;
    private CardListPanel[] bigPlayerPanels;
    private HealthPanel[] healthPanels;
    private JScrollPane[] playerPanes;
    public Viewer4(Game game){

        jFrame = newMainFrame();

        jLayeredPanel = newLayeredPane(fullRectangle());
        jFrame.add(jLayeredPanel);

        turnOffButton = newTurnOffButton(turnOffRectangle());
        jLayeredPanel.add(turnOffButton, JLayeredPane.DEFAULT_LAYER);


        shopPanel = newShopButton(shopRectangle());
        jLayeredPanel.add(shopPanel, JLayeredPane.DEFAULT_LAYER);

        bigShopPanel = new CardListPanel(game.getShop(), lesserRectangle(), base(), smallShip());
        bigShopPanel.setImage(bigShopPath);
        shopPane = horizontalPane(bigShopPanel,lesserRectangle());
        jLayeredPanel.add(shopPane,JLayeredPane.DEFAULT_LAYER);


        resourcePanel = new ResourcePanel(game.getGold(),game.getDamage(), resourceRectangle());
        jLayeredPanel.add(resourcePanel, JLayeredPane.DEFAULT_LAYER);

        //todo create LogPanel
        logPanel = new MyPanel(logRectangle());
        jLayeredPanel.add(logPanel, JLayeredPane.DEFAULT_LAYER);

        foldPanel  = newFoldButton(foldRectangle());
        jLayeredPanel.add(foldPanel, JLayeredPane.DEFAULT_LAYER);

        bigFoldPanel = new CardListPanel(game.current.getFold(),lesserRectangle(),base(),smallShip());
        bigFoldPanel.setImage(bigFoldPath);
        foldPane = horizontalPane(bigFoldPanel,lesserRectangle());
        jLayeredPanel.add(foldPane, JLayeredPane.DEFAULT_LAYER);

        bigBasesPanel = new CardListPanel(game.current.getBases(), lesserRectangle(), base(), smallShip());
        basesPane = horizontalPane(bigBasesPanel,lesserRectangle());
        jLayeredPanel.add(basesPane, JLayeredPane.DEFAULT_LAYER);
        bigBasesPanel.setImage(bigBasesPath);

        lesserPanelWatch = new LesserPanelWatch(basesPane,shopPane, foldPane);

        greaterPanel = new CardListPanel(game.current.getHand(),greaterRectangle(),base(),bigShip());
        jLayeredPanel.add(horizontalPane(greaterPanel,greaterRectangle()), JLayeredPane.DEFAULT_LAYER);
        greaterPanel.setImage(greaterPath);

        endPanel = new MyButton(endRectangle(),endPath) {
            @Override
            public void button() {
                game.nextPlayer();
                lesserPanelWatch.openBases();
            }
        };
        jLayeredPanel.add(endPanel, JLayeredPane.DEFAULT_LAYER);

        int playerC = game.getPlayers().size();
        playerPanels = new PlayerPanel[playerC];
        bigPlayerPanels = new CardListPanel[playerC];
        healthPanels = new HealthPanel[playerC];
        playerPanes = new JScrollPane[playerC];
        for (int i = 0; i < playerC; i++) {
            Player player = game.getPlayers().get(i);
            bigPlayerPanels[i] = new CardListPanel(game.getPlayers().get(i).getBases(), bigPlayerRectangle(i,playerC),base(),bigShip());
            CardListPanel bigBro = bigPlayerPanels[i];
            playerPanes[i] = verticalPane(bigBro, bigPlayerRectangle(i,playerC));
            JScrollPane scrollBro = playerPanes[i];
            healthPanels[i] = new HealthPanel(player.getHP(), game.getDamage(), heartRectangle(i,playerC),healthPath){
                @Override
                public void button(){
                    game.dealDamage(player);
                }
            };
            HealthPanel healthBro = healthPanels[i];
            playerPanels[i] = new PlayerPanel(player, playerRectangle(i, playerC)) {
                @Override
                public void button() {
                    if(!game.acceptPlayerClick(player)){
                        scrollBro.setVisible(!scrollBro.isVisible());
                        bigBro.setVisible(scrollBro.isVisible());
                        healthBro.setVisible(scrollBro.isVisible());
                    }
                }
            };
            jLayeredPanel.add(playerPanels[i], JLayeredPane.DEFAULT_LAYER);
            jLayeredPanel.add(bigBro,JLayeredPane.PALETTE_LAYER);
            jLayeredPanel.add(healthBro, JLayeredPane.PALETTE_LAYER);
            ((FlowLayout)(bigBro.getLayout())).setVgap(30);
            scrollBro.setVisible(false);
            healthBro.setVisible(false);
            bigBro.setVisible(false);
        }
    }

    private JFrame newMainFrame(){
        JFrame frame = new JFrame();
        frame.setUndecorated(true);
        frame.setExtendedState(Frame.MAXIMIZED_BOTH);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setVisible(true);
        return frame;
    }
    private JLayeredPane newLayeredPane(Rectangle rectangle){
        JLayeredPane layeredPanel = new JLayeredPane();
        layeredPanel.setLayout(null);
        layeredPanel.setBounds(rectangle);
        layeredPanel.setVisible(true);
        return layeredPanel;
    }
    private MyButton newTurnOffButton(Rectangle rectangle){
        MyButton turnOffButton = new MyButton(rectangle, turnOffPath) {
            @Override
            public void button() {
                turnOffButtonAction();
            }
        };
        return turnOffButton;
    }
    private void turnOffButtonAction(){
        System.exit(0);
    }
    private MyButton newShopButton(Rectangle rectangle){
        MyButton shopButton = new MyButton(rectangle, shopPath) {
            @Override
            public void button() {
                shopButtonAction();
            }
        };
        return shopButton;
    }
    private void shopButtonAction(){
        lesserPanelWatch.shop();
    }
    private MyButton newFoldButton(Rectangle rectangle){
        MyButton fold = new MyButton(rectangle,foldPath) {
            @Override
            public void button() {
                foldAction();
            }
        };
        return fold;
    }
    public void foldAction(){
        lesserPanelWatch.fold();
    }
    private JScrollPane horizontalPane(CardListPanel panel, Rectangle rectangle){
        JScrollPane pane = new JScrollPane(panel,ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        pane.getHorizontalScrollBar().setUI(null);
        pane.getHorizontalScrollBar().setUnitIncrement(60);
        pane.setBounds(rectangle);
        pane.setVisible(true);
        return pane;
    }
    private JScrollPane verticalPane(CardListPanel panel, Rectangle rectangle){
        JScrollPane pane = new JScrollPane(panel,ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        pane.getVerticalScrollBar().setUI(null);
        pane.getVerticalScrollBar().setUnitIncrement(60);
        pane.setBounds(rectangle);
        pane.setVisible(false);
        return pane;
    }

    private float a(){
        return 0.00f;
    }
    private float b(){
        return 0.08f;
    }
    private float g(){
        return 0.31f;
    }
    private float c(){
        return 0.41f;
    }
    private float d(){
        return 0.75f;
    }
    private float e(){
        return 0.96f;
    }
    private float f(){
        return 1.00f;
    }
    private float l() {
        return 0.00f;
    }
    private float m(){
        return 0.03f;
    }
    private float n(){
        return 0.95f;
    }
    private float o(){
        return 1.00f;
    }

    public Rectangle fullRectangle(){
        return rectum(a(),l(),o(),f());
    }
    public Rectangle turnOffRectangle(){
        return rectum(n(),a(),o(),b());
    }
    public Rectangle shopRectangle(){
        return rectum(n(),b(),o(),c());
    }
    public Rectangle lesserRectangle(){return rectum(m(),b(),n(),c());}
    public Rectangle resourceRectangle(){
        return rectum(n(),c(),o(),d());
    }
    public Rectangle logRectangle(){
        return rectum(l(),e(),n(),f());
    }
    public Rectangle foldRectangle(){
        return rectum(l(),b(),m(),c());
    }
    public Rectangle greaterRectangle(){
        return rectum(l(),c(),n(),e());
    }
    public Rectangle endRectangle(){
        return rectum(n(),d(),o(),f());
    }
    public Rectangle playerRectangle(int i, int playerCount){
        int y = (int)(jFrame.getHeight()*a());
        int w = (int)(jFrame.getWidth()*(n()-l())/playerCount);
        int h = (int)(jFrame.getHeight()*(b()-a()));
        int x = (int)(jFrame.getWidth()*l())+i*w;
        return  new Rectangle(x,y,w,h);
    }
    public Rectangle heartRectangle(int i, int playerCount){
        int y = (int)(jFrame.getHeight()*b());
        int h = (int)(jFrame.getHeight()*(g()-b()));
        int w = (int)(base().getWidth());
        int x = (int)(jFrame.getWidth()*l())+i*playerRectangle(i,playerCount).width;
        if(x + w > jFrame.getWidth())  x = jFrame.getWidth() - w;
        return new Rectangle(x,y,w,h);
    }
    public Rectangle bigPlayerRectangle(int i, int playerCount){
        int y = (int)(jFrame.getHeight()*g());
        int h = (int)(jFrame.getHeight()*(e()-g()));
        int w = (int)(base().getWidth());
        int x = (int)(jFrame.getWidth()*l())+i*playerRectangle(i,playerCount).width;
        if(x + w > jFrame.getWidth())  x = jFrame.getWidth() - w;
        return new Rectangle(x,y,w,h);
    }
    public Dimension bigShip(){
        Dimension d = base();
        return new Dimension(d.height,d.width);
    }
    public Dimension base(){
        int small = lesserRectangle().height;
        int big = (int)(small*cardPhi);
        return new Dimension(big,small);
    }
    public Dimension smallShip(){
        int big = lesserRectangle().height;
        int small = (int)(big/cardPhi);
        return new Dimension(small,big);
    }
    private Rectangle rectum(float xf, float yf, float rf, float bf){
        int width = jFrame.getWidth();
        int height = jFrame.getHeight();
        return new Rectangle((int)(xf*width),(int)(yf*height),(int)((rf-xf)*width),(int)((bf-yf)*height));
    }
}

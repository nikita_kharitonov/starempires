package mainP;

import Actions.Discard2;

import java.util.Collections;
import java.util.LinkedList;

/**
 * Created by artmmslv on 31.12.2016.
 */
public class Player {

    public Player(String name, Game g) {
        this.game = g;
        this.name = name;
        //Можно сунуть в Game или Main там где создают игрока
        //g.fillStock(stock);
        discard2 = new Discard2(this,game);//Это действие заставляет игрока скидывать карты
                                                //Активируется игрой в начале нового хода
                                                //Механика скилла желтых
    }

    public Game game;

    public boolean isCurrent() {
        return game.current.isCurrent(this);
    }

    public ObservableInteger hp = new ObservableInteger();
    public int cardsToPut = 5;//Эту тоже
    public Discard2 discard2;
    String name;//Сделать настраиваемым перед игрой

    //todo:убрать предупреждения (<>) , разобраться с багом
    private ObservableList<Card> stock = new ObservableList(new LinkedList());
    private ObservableList<Card> fold = new ObservableList(new LinkedList());
    private ObservableList<Card> hand = new ObservableList(new LinkedList());
    private ObservableList<Card> bases = new ObservableList<>(new LinkedList());

    //НАХ
    //Геттеры
    public ObservableList<Card> getHand(){ return  hand; }
    public ObservableList<Card> getFold(){ return  fold; }
    public ObservableList<Card> getBases(){ return  bases; }
    public ObservableList<Card> getStock(){return stock; }

    public String getName() {
        return name;
    }

    //todo переместить похожие методы Game и player для перемещения какрт в обно место
    //НАХ
    //Взять карты на руку
    public void toTable(){
        for (int i = 0; i < cardsToPut; i++) {
            if (stock.size() == 0)//Если карт недостаточно, помещаем фолд в сток
                foldToStock();
            if (stock.size() != 0) {
                game.stockToTable(stock.get(0));
            }
        }
    }

    //НАХ
    //Сброс замешать и добавить в конец колоды
    public void foldToStock(){
        Collections.shuffle(fold);
        int n = fold.size();
        for (int i = 0; i < n; i++) {
            game.foldToStock(fold.get(0));
        }
    }

    //НАХ
    //Карты из руки в колоду + откат способностей + отключение карт
    public  void handToFold(){
        for (int i = hand.size()-1; i >= 0; i--) {
            game.tableToFold(hand.get(i));
        }
    }

    //НАХ
    //Есть ли аванпост
    public boolean hasAvantPost(){
        for (int i = 0; i < bases.size(); i++) {
            if(bases.get(i).getDamageType()== DamageType.BLACK){
                return true;
            }
        }
        return false;
    }

    //НАХ
    //Добавляем карту в руку
    public void addCardToHand(){
        if (stock.size() == 0)
            foldToStock();
        if (stock.size() != 0) {
            game.stockToTable(stock.get(0));
        }
    }
    public ObservableInteger getHP() {
        return hp;
    }

}

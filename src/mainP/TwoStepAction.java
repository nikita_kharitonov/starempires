package mainP;


/**
 * Created by artmmslv on 17.01.2017.
 */
public abstract class TwoStepAction extends Action {
    /*Это действие с выбором карты. Методика такая:
     при клике как и у обычного действия вызывается метод launch
     Но он не вызывает метод realize, а в методе start вводит игру в особый режим принятия кликов и помечает действие активным
     При получении клика в этом особом режиме вызывается метод finish(Card),
     который, если карта подходит, реализует действие на эту карту
     если не подходит - отказывает.
      */

    ObservableBoolean active = new ObservableBoolean(false);
    public TwoStepAction(Game game){
        super(game);
        active.addObserver(this);
    }

    public void start(){
        game.setActive(this);
        setActive(true);
    }

    public void decline() {
        setActive(false);
        game.setActive(null);
    }

    public void finish(Card card){
        if(fits(card)){
            realize(card);
            setActive(false);
            setUsed(true);
            game.setActive(null);
            game.cardUtilisation(cardToDestroy);
        }else{
            decline();
        }
    }
    @Override
    protected void realize(){}//Метод realize без аргументов не нужен
    protected abstract void realize(Card card);
    protected abstract boolean fits(Card card);

    @Override
    public boolean launch(){
        //Этот метод вызывается при клике по кнопочке. Если кнопку не должно быть видно, возвращает false
        if(isDisabled()) return false;
        else
        if(game.realizingActivatedAbility()&&!isActive())return false;
        else
        if(isActive()){
            decline();
        }
        else
        if(isUsed()) game.log("Action is already Used");
        else
        if(isRestricted()) game.log("Action has a requirement");
        else
        {
            start();
        }
        return true;
    }

    public void setActive(boolean active) {
        this.active.set(active);
    }
    @Override
    public boolean isActive(){
        return active.get();
    }
    public ObservableBoolean getActive(){
        return active;
    }
}
